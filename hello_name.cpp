#include "hello_name.hpp"

#include <iostream>

void hello_name(const char* name)
{
    std::cout << "Hello, " << (name ? name : "Unknown") << '\n';
}