#include <iostream>
#include <cstring>

#include "hello_name.hpp"
#include "goodbye.hpp"

int main(int argc, char* argv[])
{
    for(int i = 1; i < argc; ++i) {
        if(std::strcmp("-n", argv[i]) == 0 || std::strcmp("--name", argv[i]) == 0) {
            if(i + 1 < argc && argv[i + 1][0] != '-') {
                hello_name(argv[++i]);
            } else {
                hello_name(nullptr);
            }
        } else if(std::strcmp("-g", argv[i]) == 0 || std::strcmp("--goodbye", argv[i]) == 0) {
            goodbye();
        } else {
            if(argv[i][0] == '-') {
                std::cerr << "[WARNING] Unknown flag '" << argv[i] << "'\n";
            } else { 
                std::cerr << "[WARNING] Unknown argument '" << argv[i] << "'\n";
            }
        }
    }
}